cmake_minimum_required(VERSION 3.1.0)

if( DEFINED CMAKE_CROSSCOMPILING )
 # subsequent toolchain loading is not really needed
 return()
endif()

if( CMAKE_TOOLCHAIN_FILE )
 # touch toolchain variable to suppress "unused variable" warning
endif()

if( DEFINED RASPBERRY_PI_TOOLCHAIN )
  message( STATUS "Raspberry Pi toolchain: " ${RASPBERRY_PI_TOOLCHAIN} )
else()
  message( FATAL_ERROR "Raspberry Pi toolchaing is not defined" )
endif()

if( RASPBERRY_PI_TOOLCHAIN )
 get_filename_component( RASPBERRY_PI_TOOLCHAIN "${RASPBERRY_PI_TOOLCHAIN}" ABSOLUTE )
 set( RASPBERRY_PI_TOOLCHAIN "${RASPBERRY_PI_TOOLCHAIN}" CACHE INTERNAL "Path of the Raspberry Pi toolchain" FORCE )
endif()

set( CMAKE_SYSTEM_VERSION 8.2 ) # Raspbian
set( CMAKE_SYSTEM_NAME Linux )
set( arch arm-linux-gnueabi  )

#set( RASPBERRY_PI_TOOLCHAIN "/Users/mac/Documents/xtools/arm-none-linux-gnueabi" )

#if( NOT CMAKE_C_COMPILER )
# if( NDK_CCACHE AND NOT ANDROID_SYSROOT MATCHES "[ ;\"]" )
#  set( CMAKE_C_COMPILER   "${NDK_CCACHE}" CACHE PATH "ccache as C compiler" )
#  set( CMAKE_CXX_COMPILER "${NDK_CCACHE}" CACHE PATH "ccache as C++ compiler" )

set( CMAKE_SYSROOT ${RASPBERRY_PI_TOOLCHAIN}/arm-none-linux-gnueabi/sysroot )
# specify the cross compiler
set( CMAKE_C_COMPILER ${RASPBERRY_PI_TOOLCHAIN}/bin/arm-none-linux-gnueabi-gcc )
set( CMAKE_C_COMPILER_TARGET ${arch} )
set( CMAKE_CXX_COMPILER ${RASPBERRY_PI_TOOLCHAIN}/bin/arm-none-linux-gnueabi-g++ )
set( CMAKE_CXX_COMPILER_TARGET ${arch} )

#include "K30Sensor.h"
#include "SensorException.h"

#include <iostream>

#ifndef HARDWARE_EMULATION
#  include <stdio.h>  // snprintf
#  include <fcntl.h>  // open
#  include <unistd.h> // close
#  include <sys/ioctl.h> // ioctl
#  include <linux/i2c-dev.h>
#  include <errno.h>
#endif

using namespace OxaSensor;

struct K30Sensor::Data {
  int fd;               /* open file descriptor: /dev/i2c-?, or -1 */
  int addr;             /* current client SMBus address */

public:
  Data()
    : fd(-1)
    , addr(-1)
  {}
};

K30Sensor::K30Sensor(unsigned inBus)
  : data_(new Data())
  , co2_(0) {
#ifndef HARDWARE_EMULATION
  int MAXPATH = 16;
  char path[MAXPATH];

  if (snprintf(path, MAXPATH, "/dev/i2c-%d", inBus) >= MAXPATH) {
    delete data_;
    throw SensorException("Failed to create i2c bus path");
  }

  data_->fd = open(path, O_RDWR, 0);
  if (data_->fd == -1) {
    delete data_;
    throw SensorException("Failed to open i2c bus");
  }

  const int adr = 0x68;
  // Set address
  if (ioctl(data_->fd, I2C_SLAVE, adr) < 0) {
    close(data_->fd);
    delete data_;
    throw SensorException("[K30Sensor] Error set address failed");
  }
#endif
}

K30Sensor::~K30Sensor() {
#ifndef HARDWARE_EMULATION
  close(data_->fd);
#endif
  delete data_;
}

bool K30Sensor::update() {
#ifndef HARDWARE_EMULATION
  usleep(25 * 1000);
  const char wdata[] = {0x22, 0x00, 0x08, 0x2A};
  const int wlen = sizeof(wdata);

  if(int x = write(data_->fd, wdata, wlen) != wlen) {
    //std::cout <<"[K30Sensor] Error write fail: "<< errno <<" " << x << std::endl;
    return false;
  }
  // device need some time to process request
  usleep(25 * 1000);       // 20 milisesonds

  char rbuf[4];
  const int rbytes = 4;
  if (int x = read(data_->fd, rbuf, rbytes) != rbytes) {
    //std::cout <<"[K30Sensor] Error read fail: " << errno <<" "<< x << std::endl;
    return false;
  }

  const unsigned co2 = (rbuf[1] * 256) + rbuf[2];
  const int sym = rbuf[0] + rbuf[1] + rbuf[2];
  const bool done = sym == rbuf[3];
  if (done) {
    co2_ = co2;
    //	std::cout<<"[K30Sensor] Update complete" << std::endl;
  } else {
    //std::cout<<"[K30Sensr] invalid checksum"<<std::endl;
  }
#else
  const bool done = true;
  co2_ = 850;
#endif
  return done;
}

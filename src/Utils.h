#pragma once

namespace OxaSensor {

// Wait for some number of milliseconds
void delay(unsigned int howLong);
void delayMicroseconds(unsigned int howLong);

} // namespace OxaSensor

#pragma once

#include <iostream>

namespace OxaSensor {

class DHTSensor {
public:
  enum Type {
    eDHT11 = 11,
    eDHT22 = 22
  };

public:
  DHTSensor(Type inType, unsigned inGPIOPin);
  ~DHTSensor();

  float getHumidity() const { return humidity_; }
  float getTemperature() const { return temperature_; }

  bool update();

private:
  Type type_;
  unsigned gpioPin_;
  float humidity_;
  float temperature_;
  int bits_[250];
  int data_[100];
};

//
std::ostream& operator << (std::ostream& out, DHTSensor::Type inType);
}

#pragma once

#include <string>
#include <sstream>
#include <iostream>

#include "SFML/Network/Http.hpp"

namespace OxaSensor {

template<class T>
class Reporter {
public:
  Reporter(const std::string& host,
           const std::string& uri,
           unsigned port) {
    http_.setHost(host, port);
    request_.setUri(uri);
    request_.setMethod(sf::Http::Request::Post);
    request_.setField("Content-type", "application/json");
  }

  void setPayload(const T& payload) {
    payload_ = payload;
  }

  bool update() {
    oStream.clear();
    oStream.str("");

    oStream << payload_;

    std::cout << oStream.str() << std::endl;

    request_.setBody(oStream.str());
    sf::Http::Response response = http_.sendRequest(request_, sf::seconds(5));

    std::cout << "status: " << response.getStatus() << std::endl;
    std::cout << "HTTP version: " << response.getMajorHttpVersion() << "." << response.getMinorHttpVersion() <<
      std::endl;
    std::cout << "Content-Type header:" << response.getField("Content-Type") << std::endl;
    std::cout << "body: " << response.getBody() << std::endl;
    std::cout << "repost is " << (response.getStatus() == 200 ? "OK" : "FAILED") << std::endl;
    return response.getStatus() == 200;
  }

private:
  sf::Http http_;
  sf::Http::Request request_;
  std::ostringstream oStream;
  T payload_;
};

}

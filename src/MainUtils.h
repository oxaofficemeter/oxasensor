#pragma once

#include <iostream>
#include <fstream>
#include "JSONValue.h"

#define DEFAULT_SETTINGS_PATH "/Users/pavel/Work/OfficeMeter/settings.ini"
#define DEFAULT_HOST "http://officemeter.oxagile.com"
#define DEFAULT_URI "/sensor"
#define DEFAULT_UPDATE_TIMEOUTSEC 15
#define DEFAULT_REPORT_TIMEOUTSEC 30
#define DEFAULT_SLEEP_TIMEOUTSEC 1


namespace OxaSensor {

struct SensorPayload {
  std::string deviceUid;
  float temperature;
  float humidity;
  unsigned co2;
};

std::ostream& operator << (std::ostream& stream, const SensorPayload& payload) {
  stream << "{"
         << "\"deviceUid\":\""   << payload.deviceUid << "\", "
         << "\"temperature\":\"" << payload.temperature << "\", "
         << "\"humidity\":\""    << payload.humidity << "\", "
         << "\"co2\":\""         << payload.co2 << "\""
         << "}";
  return stream;
}

std::string getDeviceUid() {
  std::ifstream cpuinfo("/proc/cpuinfo");
  if( !cpuinfo.is_open())
    return "rpi-emulation-12346";

  std::string line;
  while(!cpuinfo.eof()) {
    std::getline(cpuinfo, line);
    if (line.substr(0, 6) == "Serial") {
      const size_t serial_pos = line.find(":") + 2;
      return line.substr(serial_pos);
      break;
    }
  }
  return "rpi-emulation-error";
}

struct ProgramSettings {
  std::string deviceUid;
  std::string host;
  std::string uri;
  unsigned updateTimeoutSec;
  unsigned reportTimeoutSec;
  unsigned sleepTimeoutSec;

  ProgramSettings() {
    deviceUid = getDeviceUid();
    host = DEFAULT_HOST;
    uri  = DEFAULT_URI;
    updateTimeoutSec = DEFAULT_UPDATE_TIMEOUTSEC;
    reportTimeoutSec = DEFAULT_REPORT_TIMEOUTSEC;
    sleepTimeoutSec = DEFAULT_SLEEP_TIMEOUTSEC;
  }
};

std::string w2str(std::wstring wstr) {
  return std::string(wstr.begin(), wstr.end());
}

ProgramSettings loadSettings(std::string& settingsPath) {
  ProgramSettings settings;
  if(settingsPath.length() == 0)
    settingsPath = DEFAULT_SETTINGS_PATH;
  std::ifstream fileSettings;
  fileSettings.open (settingsPath.c_str());
  if(fileSettings.is_open()) {
    std::string temp, text;
    while (fileSettings >> temp) {
      text += temp;
    }

    fileSettings.close();

    JSONValue* rootValue = JSON::Parse(text.c_str());
    if(rootValue) {
      if(rootValue->IsObject()) {
        JSONObject rootObject = rootValue->AsObject();
        if (rootObject.find(L"host") != rootObject.end() && rootObject[L"host"]->IsString()) {
          settings.host = w2str(rootObject[L"host"]->AsString());
        }
        if (rootObject.find(L"uri") != rootObject.end() && rootObject[L"uri"]->IsString()) {
          settings.uri = w2str(rootObject[L"uri"]->AsString());
        }
        if (rootObject.find(L"updateTimeoutSec") != rootObject.end()
            && rootObject[L"updateTimeoutSec"]->IsNumber()) {
          settings.updateTimeoutSec = rootObject[L"updateTimeoutSec"]->AsNumber();
        }
        if (rootObject.find(L"reportTimeoutSec") != rootObject.end() &&
            rootObject[L"reportTimeoutSec"]->IsNumber()) {
          settings.reportTimeoutSec = rootObject[L"reportTimeoutSec"]->AsNumber();
        }
        if (rootObject.find(L"sleepTimeoutSec") != rootObject.end() &&
            rootObject[L"sleepTimeoutSec"]->IsNumber()) {
          settings.uri = rootObject[L"sleepTimeoutSec"]->AsNumber();
        }
      }
      delete rootValue;
    }
  }
  return settings;
}
} // namespace OxaSensor

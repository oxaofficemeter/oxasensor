#pragma once

namespace OxaSensor {

class K30Sensor {
public:
  K30Sensor(unsigned inBus);
  ~K30Sensor();

  bool update();
  unsigned getCO2() const { return co2_; }

private:
  K30Sensor(const K30Sensor&);
  void operator = (const K30Sensor&);

private:
  struct Data;

private:
  Data* data_;
  unsigned co2_;
};
}

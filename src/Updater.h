#pragma once

#include <sys/time.h>

namespace OxaSensor {
namespace impl {
int timeval_subtract(struct timeval* result, struct timeval* t2, struct timeval* t1) {
  long int diff = (t2->tv_usec + 1000000 * t2->tv_sec) - (t1->tv_usec + 1000000 * t1->tv_sec);
  result->tv_sec = diff / 1000000;
  result->tv_usec = diff % 1000000;

  return (diff < 0);
}
} //namespace impl

/**
 * this object is used to peform regular update for object with given timeout
 * and retry count
 */
template<class T>
class Updater {
public:
  Updater(T& object, unsigned updateTimeOutSec, unsigned retryCount = 1000)
    : object_(object)
    , updateTimeOutSec_(updateTimeOutSec)
    , retryCount_(retryCount) {

    tvLastUpdate_.tv_sec = 0;
    tvLastUpdate_.tv_usec = 0;
  }

  bool update() {
    struct timeval tvNow, tvDiff;
    gettimeofday(&tvNow, NULL);
    impl::timeval_subtract(&tvDiff, &tvNow, &tvLastUpdate_);
    if (tvDiff.tv_sec >= updateTimeOutSec_) {
      int count = retryCount_;
      while (count) {
        const bool success = object_.update();
        if (success) {
          gettimeofday(&tvLastUpdate_, NULL);
          return true;
        }
        --count;
      }
    }
    return false;
  }

private:
  T& object_;
  const unsigned updateTimeOutSec_;
  struct timeval tvLastUpdate_;
  const unsigned retryCount_;
};
} // namespace OxaSensor

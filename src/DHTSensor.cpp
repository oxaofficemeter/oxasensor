#include "DHTSensor.h"
#include "SensorException.h"

#ifndef HARDWARE_EMULATION
#  include <bcm2835.h>
#  include <unistd.h>
#endif

#include <assert.h>
#ifdef DEBUG
#  include <stdio.h>
#endif

#define MAXTIMINGS 100

namespace {

uint8_t sizecvt(const int read) {
  /* digitalRead() and friends from wiringpi are defined as returning a value
     < 256. However, they are returned as int() types. This is a safety function */

  if (read > 255 || read < 0) {
    printf("Invalid data from wiringPi library\n");
    exit(EXIT_FAILURE);
  }
  return (uint8_t)read;
}
} // namepsace


namespace OxaSensor {
std::ostream& operator << (std::ostream& out, DHTSensor::Type inType) {
  switch (inType) {
  case DHTSensor::eDHT11: out << "DHT11";
    break;
  case DHTSensor::eDHT22: out << "DHT22";
    break;
  default:
    out << "Unknown";
    break;
  }
  return out;
}
}

using namespace OxaSensor;

DHTSensor::DHTSensor(DHTSensor::Type inType, unsigned inGPIOPin)
  : type_(inType)
  , gpioPin_(inGPIOPin)
  , humidity_ (0.f)
  , temperature_ (0.f) {
  std::cout << "[DHTSensor] initialize type" << type_ << " GPIO pin: " << gpioPin_ << std::endl;

#ifndef HARDWARE_EMULATION
  if (!bcm2835_init())
    throw SensorException("Failed to initialize bcm2835 library");
#endif
  std::cout << "[DHTSensor] initialization complete" << std::endl;
}

DHTSensor::~DHTSensor() {}

bool DHTSensor::update() {
#ifndef HARDWARE_EMULATION
  //std::cout <<"[DHTSensor] update" << std::endl;
  int bitidx = 0;
  int j = 0;

  int laststate = HIGH;
  data_[0] = data_[1] = data_[2] = data_[3] = data_[4] = 0;

  // Set GPIO pin to output
  bcm2835_gpio_fsel(gpioPin_, BCM2835_GPIO_FSEL_OUTP);
  bcm2835_gpio_write(gpioPin_, HIGH);
  delay(10);
  bcm2835_gpio_write(gpioPin_, LOW);
  delay(18);
  // then pull it up for 40 microseconds
  bcm2835_gpio_write(gpioPin_, HIGH);
  delayMicroseconds(40);
  // prepare pin for read
  bcm2835_gpio_fsel(gpioPin_, BCM2835_GPIO_FSEL_INPT);

  // detect change and read data
  for (int i = 0, counter = 0; i < MAXTIMINGS; i++) {
    counter = 0;
    while (bcm2835_gpio_lev(gpioPin_) == laststate) {
      counter++;
      delayMicroseconds(1);
      if (counter == 255) {
        break;
      }
    }

    laststate = bcm2835_gpio_lev(gpioPin_);

    if (counter == 255) break;

    // ignore first 3 transitions
    if ((i >= 4) && (i % 2 == 0)) {
      // shove each bit into the storage bytes
      data_[j / 8] <<= 1;
      if (counter > 16)
        data_[j / 8] |= 1;
      j++;
    }
  }


  // read data!
  /*
     for (int i = 0; i < MAXTIMINGS; i++) {
     int counter = 0;
     while ( bcm2835_gpio_lev(gpioPin_) == laststate) {
      counter++;
      bcm2835_delayMicroseconds(1);
      if (counter == 100)
        break;
     }

     laststate = bcm2835_gpio_lev(gpioPin_);
     bits_[bitidx++] = counter;

     if ((i > 3) && (i % 2 == 0)) {
      // shove each bit into the storage bytes
      data_[j / 8] <<= 1;
      if (counter > 16)
        data_[j / 8] |= 1;
      j++;
     }
     }
   */

#ifdef DEBUG
  printf("Data (%d): 0x%x 0x%x 0x%x 0x%x 0x%x\n", j, data[0], data[1], data[2], data[3], data[4]);
#endif
  const int checksum = (data_[0] + data_[1] + data_[2] + data_[3]) & 0xFF;
  const bool succesed = (j >= 39) && (data_[4] == checksum);

  float humidity = 0.f;
  float temperature = 0.f;

  if (succesed) {
    switch (type_) {
    case eDHT11:
      temperature = static_cast<float> (data_[2]);
      humidity    = static_cast<float> (data_[0]);
      break;

    case eDHT22:
    {
      humidity = (data_[0] * 256 + data_[1]) / 10.f;
      temperature = ( (data_[2] & 0x7F) * 256 + data_[3]) / 10.f;
      if (data_[2] & 0x80)
        temperature *= -1.f;
      break;
    }

    default:
      assert(0 && "[DHTSensor] Unknown sensor type.");
    }

    if (temperature >= -40.f && temperature <= 80.f)
      temperature_ = temperature;
    else
      return false;

    if (humidity >= 0.f && humidity <= 100.f)
      humidity_ = humidity;
    else
      return false;
  }
#else
  const bool succesed = true;
  temperature_ = 24.3001f;
  humidity_    = 31.4001f;
#endif
  // std::cout << "[DHTSensor] update complete: " << succesed << std::endl;
  return succesed;
}

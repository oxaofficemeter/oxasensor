#include <iostream>
#include <fstream>

#include <unistd.h> // usleep
#include <pthread.h>

#include <spdlog/spdlog.h>

#include "DHTSensor.h"
#include "K30Sensor.h"
#include "Updater.h"
#include "Reporter.h"
#include "MainUtils.h"

using namespace OxaSensor;

namespace {
const std::string version = "0.1.2";
}

int main(int argc, const char* argv[]) {
  auto app = spdlog::stdout_logger_mt("app");
  // set thread priority
  {
    int policy;
    struct sched_param param;
    pthread_getschedparam(pthread_self(), &policy, &param);
    param.sched_priority = sched_get_priority_max(policy);
    pthread_setschedparam(pthread_self(), policy, &param);
  }

  try {
    std::string param1("");
    std::string configFile;
    if(argc > 1) {
      param1 = argv[1];
    }

    do {
      if (argc == 2 && param1 == "--help") {
        std::cout << "Oxasensor " << std::endl;
        std::cout << " --version - Print current binary version" << std::endl;
        std::cout << " --config file - User given config file" << std::endl;
        return 0;
      }

      if (argc == 2 && param1 == "--version") {
        std::cout << "Version: " << version << std::endl;
        std::cout << "Build: " << __DATE__ << " " << __TIME__ << std::endl;
        return 0;
      }

      if (argc == 3 && param1 == "--config") {
        configFile = argv[2];
        app->info("config file: {0}", configFile);
        break;
      }

      if (argc > 1) {
        std::cout << "Invalid parameters" << std::endl;
        return 1;
      }
    } while(0);

    const ProgramSettings settings = loadSettings(configFile);
    app->info("DeviceUid: {0}", settings.deviceUid);

    DHTSensor dht(DHTSensor::eDHT22, 4);
    K30Sensor k30(1);
    Reporter<SensorPayload> reporter(settings.host, settings.uri, 4000);

    Updater<DHTSensor> dhtUpdater(dht, settings.updateTimeoutSec);
    Updater<K30Sensor> k30Updater(k30, settings.updateTimeoutSec);
    Updater<Reporter<SensorPayload> > reporterUpdater(reporter, settings.reportTimeoutSec, 3 /*retury count*/);

    SensorPayload payload;
    payload.deviceUid = settings.deviceUid;

    while (1) {
      dhtUpdater.update();
      k30Updater.update();

      payload.temperature = dht.getTemperature();
      payload.humidity = dht.getHumidity();
      payload.co2 = k30.getCO2();

      reporter.setPayload(payload);
      /*auto success = */ reporterUpdater.update();
      sleep(settings.sleepTimeoutSec);
    }
    return 0;
  }
  catch(const std::exception& ex) {
    app->error("Exception: {0}", ex.what());
  }
  return 1;
}

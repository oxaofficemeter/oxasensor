#pragma once

#include <exception>
#include <string>

namespace OxaSensor {

class SensorException : public std::exception {
public:
  explicit SensorException(const char* inWhat) : what_(inWhat) {}
  virtual ~SensorException() noexcept {}

  virtual const char* what() const noexcept {
    return what_.c_str();
  }
private:
  std::string what_;
};
} // namespace OxaSensor

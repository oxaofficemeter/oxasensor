# README #

This repository contains source code for Oxagile Office Sensor.

### Configure

we are using cmake to create project files.
Configure project from cmake-gui or using command line.

To use interactive configuration mode execute ccmake insted of cmake.

### Compiling

to compile project executable

```
#!shell
mkdir build
cd build 
cmake ..
make
```
Hardware emulation mode use on by default for OS X or can be enabled as make parameter:
```
#!shell
cmake -DOXA_HARDWARE_EMULATION=ON ..
```

To generate debian package execute CPack after project was built:
```
#!shell
cpack -G DEB 
```